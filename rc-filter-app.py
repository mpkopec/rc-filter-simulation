#!/usr/bin/env python
# coding=utf-8

from PySide import QtCore, QtGui
from Config import *
from RCFilter import RCFilter
import MainWindow
import sys

def filterErrorBox(text, extendedText = ""):
	msgBox = QtGui.QMessageBox()
	msgBox.setWindowTitle("Error!")
	msgBox.setText(text)
	msgBox.setDetailedText(extendedText)
	msgBox.exec_()

def filterInfoBox(text, extendedText = ""):
	msgBox = QtGui.QMessageBox()
	msgBox.setWindowTitle("Info")
	msgBox.setText(text)
	msgBox.setDetailedText(extendedText)
	msgBox.exec_()

def debug():
	print config.__dict__

config = FilterSimulationConfig()
filter = RCFilter(config.R, config.C)
filter.setUinFunction(config.getInputFunction())
config.method = filter.solveDifferentialEquationRungeKutta4


class GUIWindow(QtGui.QMainWindow):
	__mw = None

	def __init__(self):
		super(GUIWindow, self).__init__()
		self.__mw = MainWindow.Ui_MainWindow()
		self.__mw.setupUi(self)
		self.setGeometry(100, 100, self.width(), self.height())
		self.__mw.figure.suptitle("RC filter simulation")

		# Generating GUI values from default config
		self.__mw.lineEdit_5.setText(str(config.R))
		self.__mw.lineEdit_6.setText(str(config.C))

		self.__mw.lineEdit_4.setText(str(config.timeStep))
		self.__mw.lineEdit_7.setText(str(config.stopTime))

		if config.inputFunction == filterSine:
			self.__mw.InputFunction_2.setCurrentIndex(0)
			self.__mw.lineEdit_8.setText(str(config.inputFunctionParams[0]))
			self.__mw.lineEdit_9.setText(str(config.inputFunctionParams[1]))
		else:
			self.__mw.InputFunction_2.setCurrentIndex(1)
			self.__mw.lineEdit_10.setText(str(config.inputFunctionParams[0]))
			self.__mw.lineEdit_11.setText(str(config.inputFunctionParams[1]))

		if config.method == filter.solveDifferentialEquationEuler:
			self.__mw.comboBox.setCurrentIndex(0)
		else:
			self.__mw.comboBox.setCurrentIndex(1)

		# Show the window
		self.show()

		#--------------------------------------------------
		#--------- Connecting signals with slots ----------
		#--------------------------------------------------

		self.__mw.lineEdit_5.editingFinished.connect(self.resistanceChanged)
		self.__mw.lineEdit_6.editingFinished.connect(self.capacitanceChanged)

		self.__mw.comboBox.currentIndexChanged.connect(self.methodChanged)
		self.__mw.lineEdit_4.editingFinished.connect(self.timeStepChanged)
		self.__mw.lineEdit_7.editingFinished.connect(self.stopTimeChanged)

		self.__mw.radioButton_7.clicked.connect(self.sweepR)
		self.__mw.radioButton_8.clicked.connect(self.sweepC)
		self.__mw.radioButton_9.clicked.connect(self.sweepNone)
		self.__mw.lineEdit.editingFinished.connect(self.sweepMinChanged)
		self.__mw.lineEdit_2.editingFinished.connect(self.sweepMaxChanged)
		self.__mw.lineEdit_3.editingFinished.connect(self.sweepStepsChanged)

		self.__mw.InputFunction_2.currentChanged.connect(self.tabChanged)

		self.__mw.lineEdit_8.editingFinished.connect(self.sineFreqChanged)
		self.__mw.lineEdit_9.editingFinished.connect(self.sineAmplChanged)
		self.__mw.lineEdit_10.editingFinished.connect(self.squareWidthChanged)
		self.__mw.lineEdit_11.editingFinished.connect(self.squareAmplitudeChanged)

		self.__mw.actionZapisz_wykres.triggered.connect(self.saveFigure)
		self.__mw.actionZamknij.triggered.connect(self.closeWindow)
		self.__mw.actionAbout.triggered.connect(self.about)

		self.__mw.pushButton.clicked.connect(self.simulateAction)

		# MainWindow.plotCanvas.plot([0, 1])
		# self.__mw.chart.draw()

	def about(self):
		QtGui.QMessageBox.about(self, "About", "Created by Maciej Kopec \n\n"
											   "This program simulates RC filter response for step signal and"
											   "sine wave. It also can sweep R or C parameters for seeing the"
											   "big scale changes. Sweep output is a colormap.\n"
											   "This version contains debug information printing feature.\n\n"
											   "Version 1.0b")

	def closeWindow(self):
		self.close()

	def saveFigure(self):
		filename = QtGui.QFileDialog.getSaveFileName(self, "Save image as a *.png file", filter="Image Files (*.png)")

		try:
			self.__mw.figure.savefig(str(filename[0]))
			filterInfoBox("File saved!")
		except Exception:
			filterErrorBox("Error writing file!")

	def simulateAction(self):
		steps = int(config.stopTime / config.timeStep)

		Uin = config.getInputFunction()
		filter.setUinFunction(Uin)
		self.__mw.progressBar_2.setValue(0)

		if not config.sweep:
			xsolv, ysolv = config.method(steps, config.timeStep, 0, self.__mw.progressBar.setValue)
			yin = [Uin(x) for x in xsolv]
			self.__mw.figure.clf()
			plot = self.__mw.figure.add_subplot(111, xlabel='Time [s]', ylabel='Amplitude [V]')
			plot.cla()
			plot.set_xlabel("Time [s]")
			plot.set_ylabel("Amplitude [V]")
			plot.set_title("RC filter simulation")
			plot.plot( xsolv, yin, 'b-', label='Input' )
			plot.plot( xsolv, ysolv, 'r-', label="Simulated")
			plot.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=1, ncol=2, borderaxespad=0.)
			self.__mw.chart.draw()
			self.__mw.progressBar_2.setValue(1/1 * 100)
		else:
			changeParamFunction = None
			if config.sweep == 'R':
				changeParamFunction = filter.setR
			else:
				changeParamFunction = filter.setC

			sweepStep = (config.sweepMax - config.sweepMin)/config.sweepSteps
			stepsList = [config.sweepMin + i*sweepStep for i in range(config.sweepSteps)]
			tList = []
			cList = []
			cListIn = []
			vList = []

			for i, val in enumerate(stepsList):
				changeParamFunction(val)

				vList.extend([val] * steps)
				x, y = config.method(steps, config.timeStep, 0, self.__mw.progressBar.setValue)
				tList.extend(x)
				cList.extend(y)
				cListIn.extend([Uin(yin) for yin in x])

				self.__mw.progressBar_2.setValue(100 * i/(config.sweepSteps - 1))

			self.__mw.figure.clf()

			plot1 = self.__mw.figure.add_subplot(121, title="Simulated function", xlabel='Time [s]', ylabel=r'Resistance [$\Omega$]' if config.sweep == 'R' else r'Capacitance [F]')
			plot1.scatter( label="Uout", x=tList, y=vList, c=cList, marker=',', s=4, edgecolors='none' )
			plot1.set_ylim(min(vList), max(vList))
			plot1.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
			plot1.ticklabel_format(axis='x', style='sci', scilimits=(-2,2))
			plot1.legend()

			plot2 = self.__mw.figure.add_subplot(122, title="Input function", xlabel='Time [s]', ylabel=r'Resistance [$\Omega$]' if config.sweep == 'R' else r'Capacitance [F]')
			plot2.scatter( label="Uin", x=tList, y=vList, c=cListIn, marker=',', s=4, edgecolors='none' )
			plot2.set_ylim(min(vList), max(vList))
			plot2.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
			plot2.ticklabel_format(axis='x', style='sci', scilimits=(-2,2))
			plot2.legend()

			self.__mw.chart.draw()

	def sweepR(self):
		config.sweep = 'R'
		self.__mw.lineEdit.setEnabled(True)
		self.__mw.lineEdit_2.setEnabled(True)
		self.__mw.lineEdit_3.setEnabled(True)
		debug()

	def sweepC(self):
		config.sweep = 'C'
		self.__mw.lineEdit.setEnabled(True)
		self.__mw.lineEdit_2.setEnabled(True)
		self.__mw.lineEdit_3.setEnabled(True)
		debug()

	def sweepNone(self):
		config.sweep = None
		self.__mw.lineEdit.setEnabled(False)
		self.__mw.lineEdit_2.setEnabled(False)
		self.__mw.lineEdit_3.setEnabled(False)
		debug()

	def sweepMinChanged(self):
		smin = filterIsFloat(self.__mw.lineEdit.text())

		if smin is not False:
			config.sweepMin = smin
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def sweepMaxChanged(self):
		smax = filterIsFloat(self.__mw.lineEdit_2.text())

		if smax is not False:
			config.sweepMax = smax
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def sweepStepsChanged(self):
		ssteps = filterIsInt(self.__mw.lineEdit_3.text())

		if ssteps is not False:
			config.sweepSteps = ssteps
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def tabChanged(self):
		if self.__mw.InputFunction_2.currentIndex() == 0:
			config.inputFunction = filterSine
		else:
			config.inputFunction = filterSquare

	def methodChanged(self):
		if self.__mw.comboBox.currentIndex() == 0:
			config.method = filter.solveDifferentialEquationEuler
		else:
			config.method = filter.solveDifferentialEquationRungeKutta4
		debug()

	def timeStepChanged(self):
		step = filterIsFloat(self.__mw.lineEdit_4.text())

		if step is not False:
			config.timeStep = step
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def stopTimeChanged(self):
		stop = filterIsFloat(self.__mw.lineEdit_7.text())

		if stop is not False:
			config.stopTime = stop
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def resistanceChanged(self):
		res = filterIsFloat(self.__mw.lineEdit_5.text())

		if res is not False:
			config.R = res
			filter.setR(config.R)
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def capacitanceChanged(self):
		cap = filterIsFloat(self.__mw.lineEdit_6.text())

		if cap is not False:
			config.C = cap
			filter.setC(config.C)
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def squareAmplitudeChanged(self):
		ampl = filterIsFloat(self.__mw.lineEdit_11.text())

		if ampl is not False:
			config.inputFunctionParams[1] = ampl
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def squareWidthChanged(self):
		width = filterIsFloat(self.__mw.lineEdit_10.text())

		if width is not False:
			config.inputFunctionParams[0] = width
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def sineAmplChanged(self):
		ampl = filterIsFloat(self.__mw.lineEdit_9.text())

		if ampl is not False:
			config.inputFunctionParams[1] = ampl
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")
		debug()

	def sineFreqChanged(self):
		freq = filterIsFloat(self.__mw.lineEdit_8.text())

		if freq is not False:
			config.inputFunctionParams[0] = freq
		else:
			filterErrorBox("Entered value is not a number!", "Please give a number in this field.")


app = QtGui.QApplication(sys.argv)
window = GUIWindow()
app.processEvents()
sys.exit(app.exec_())