#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

def filterIsFloat(x):
	try:
		return float(x)
	except ValueError:
		return False

def filterIsInt(x):
	try:
		return int(x)
	except ValueError:
		return False

def filterSquare(x, fallMoment = 1, amplitude = 1):
	if x == 0 or x >= fallMoment:
		return 0
	else:
		return amplitude

def filterSine(x, frequency = 1e3, amplitude = 1):
	return amplitude * math.sin(x * 2*math.pi*frequency)


class FilterSimulationConfig:
	def __init__(self):
		self.R = 1e3
		self.C = 1e-6
		self.method = None
		self.timeStep = self.R * self.C * 1/100
		self.stopTime = self.R * self.C * 10
		self.sweep = None
		self.sweepMin = None
		self.sweepMax = None
		self.sweepSteps = None
		self.inputFunction = filterSine
		self.inputFunctionParams = [1e3, 1]

	def getInputFunction(self):
		def tempf (x):
			return self.inputFunction(x, self.inputFunctionParams[0], self.inputFunctionParams[1])
		return tempf