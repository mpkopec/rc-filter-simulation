#!/usr/bin/env python
# -*- coding: utf-8 -*-

class RCFilter:
	""" RC filter class, designed for containing
	and simulating an RC filter. It solves differential
	equation associated with RC circuit and returns
	values as a dictionary where index is argument (time)
	and value is output voltage. """

	__R = None
	__C = None
	__UinFunction = None

	def	__init__( self, R, C ):
		self.__R = R
		self.__C = C


	def setUinFunction( self, UinFunction ):
		def temp ( t, Uout ):
			return (1/( self.__R * self.__C ))*( UinFunction( t ) - Uout )

		self.__UinFunction = temp


	def getUinFunction( self ):
		return self.__UinFunction


	def getTau ( self ):
		return self.__C*self.__R


	def getR ( self ):
		return self.__R


	def getC ( self ):
		return self.__C


	def printUinFunction( self ):
		print self.__UinFunction


	def setC ( self, C ):
		self.__C = C


	def setR ( self, R ):
		self.__R = R


	def setRC( self, R, C ):
		self.__R = R
		self.__C = C


	def solveDifferentialEquationEuler( self, steps, stepSize, y0, setBarValue ):
		x = [0.0]
		y = [y0]

		for i in range( steps - 1 ):
			setBarValue(100 * i/(steps - 2))
			x.append((i+1)*stepSize)
			y.append(y[i] + stepSize*self.__UinFunction(i*stepSize, y[i]))

		return x, y


	def solveDifferentialEquationRungeKutta4( self, steps, stepSize, y0, setBarValue ):
		x = [0.0]
		y = [y0]

		for i in range( steps - 1 ):
			setBarValue(100 * i/(steps - 2))
			k1 = stepSize*self.__UinFunction( x[i], y[i] )
			k2 = stepSize*self.__UinFunction( x[i] + stepSize/2, y[i] + k1/2 )
			k3 = stepSize*self.__UinFunction( x[i] + stepSize/2, y[i] + k2/2 )
			k4 = stepSize*self.__UinFunction( x[i] + stepSize, y[i] + k3 )

			x.append((i+1)*stepSize)
			y.append(y[i] + (k1 + 2*k2 + 2*k3 + k4)/6)

		return x, y
